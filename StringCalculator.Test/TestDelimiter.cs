﻿using NUnit.Framework;
using StringCalculator.Source;

namespace StringCalculator.Test
{
    [TestFixture]
    public class TestDelimiter
    {
        private Delimiter _delimiter;

        [OneTimeSetUp]
        public void Initialize()
        {
            _delimiter = new Delimiter();
        }

        [Test]
        public void GIVEN_NumbersWithDefaultDelimiter_WHEN_GettingDelimiters_THEN_ReturnDefaultDelimiters()
        {
            var expected = new[] { ",", "\n" };
            var actual = _delimiter.GetDelimiters("1,2\n6");

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GIVEN_NumbersWithCustomDelimiter_WHEN_GettingDelimiters_THEN_ReturnCustomDelimiter()
        {
            var expected = new[] { "*" };
            var actual = _delimiter.GetDelimiters("//*\n1*2*3");

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GIVEN_NumbersWithMultpleCustomDelimiter_WHEN_GettingDelimiters_THEN_ReturnMultipleCustomDelimiters()
        {
            var expected = new[] { "*", "&" };
            var actual = _delimiter.GetDelimiters("//[*][&]\n1*2&3");

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GIVEN_Delimiters_WHEN_GettingSortedDelimiters_THEN_ReturnSortedDelimiters()
        {
            var expected = new[] { "$$f", "ff$", "$f", "f$", "$" };
            var actual = _delimiter.GetSortedDelimiters(new[] { "$", "$$f", "$f", "ff$", "f$" });

            Assert.AreEqual(expected,actual);
        }
    }
}
