﻿using NUnit.Framework;
using StringCalculator.Source;
using System;
using System.Collections.Generic;

namespace StringCalculator.Test
{
    [TestFixture]
    public class TestNumber
    {
        private Number _number;

        [OneTimeSetUp]
        public void initialize()
        {
            _number = new Number();
        }

        [Test]
        public void GIVEN_NumbersWithCustomDelimiters_WHEN_GetNumberSection_THEN_ReturnNumberSection()
        {
            var expected = "1;2;3";
            var actual = _number.GetNumberSection("//;\n1;2;3");

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GIVEN_NumbersWithDefaultDelimiters_WHEN_GetNumberSection_THEN_ReturnNumbers()
        {
            var expected = "1,2,3";
            var actual = _number.GetNumberSection("1,2,3");

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GIVEN_NumberSectionAndDelimiters_WHEN_GetNumber_THEN_ReturnNumbersList()
        {
            var expected = new List<int> { 1, 2, 3 };
            var actual = _number.GetNumbers("1$2*3", new[] { "$", "*" });

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GIVEN_NumbersListWithNegativeNumbers_WHEN_Validatingnumbers_THEN_ThrowException()
        {
            var expected = "Negative not allowed -8, -9";
            var actual = Assert.Throws<Exception>(() => _number.ValidateNumbers(new List<int> { 1, 2, 3, -8, -9 }));

            Assert.AreEqual(expected, actual.Message);
        }
    }
}
