﻿using NUnit.Framework;
using StringCalculator.Functionality;
using System.Collections.Generic;

namespace StringCalculator.Test
{
    [TestFixture]
    public class TestAddition
    {
        private Addition _addition;

        [OneTimeSetUp]
        public void Initialize()
        {
            _addition = new Addition();
        }

        [Test]
        public void GIVEN_NumbersList_WHEN_DoingAddition_THEN_ReturnSum()
        {
            var expected = 126;
            var actual = _addition.GetSum(new List<int> { 12, 1080, 36, 78 });

            Assert.AreEqual(expected, actual);
        }
    }
}
