﻿using System.Collections.Generic;

namespace StringCalculator.Functionality
{
    public class Addition
    {
        public int GetSum(List<int> numbersList)
        {
            int sum = 0;

            foreach (var number in numbersList)
            {
                if (number < 1001)
                {
                    sum += number;
                }
            }

            return sum;
        }
    }
}
