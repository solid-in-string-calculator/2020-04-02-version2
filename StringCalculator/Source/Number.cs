﻿using System;
using System.Collections.Generic;

namespace StringCalculator.Source
{
    public class Number
    {
        public string GetNumberSection(string numbers)
        {
            var customDelmitersId = "//";
            var newline = "\n";

            if (numbers.StartsWith(customDelmitersId))
            {
                return numbers.Substring(numbers.IndexOf(newline) + 1);
            }

            return numbers;
        }

        public void ValidateNumbers(List<int> numbersList)
        {
            List<string> negativeNumber = new List<string>();

            foreach (var number in numbersList)
            {
                if (number < 0)
                {
                    negativeNumber.Add(number.ToString());
                }
            }

            if (negativeNumber.Count != 0)
            {
                throw new Exception($"Negative not allowed {string.Join(", ", negativeNumber)}");
            }
        }

        public List<int> GetNumbers(string numberSection, string[] delimiters)
        {
            List<int> numbersList = new List<int>();
            string[] numbersArray = numberSection.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);

            foreach (var num in numbersArray)
            {
                if (int.TryParse(num, out int number))
                {
                    numbersList.Add(number);
                }
            }

            return numbersList;
        }
    }
}
