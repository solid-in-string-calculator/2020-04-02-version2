﻿using System;

namespace StringCalculator.Source
{
    public class Delimiter
    {
        public string[] GetDelimiters(string numbers)
        {
            var customDelimiterId = "//";
            var newline = "\n";
            var multipleCustomDelimitersId = $"{customDelimiterId}[";
            var multipleCustomDelimitersSeperator = $"]{newline}";
            var multipleCustomDelimitersSplitter = "][";

            if (numbers.StartsWith(multipleCustomDelimitersId))
            {
                var delimiters = numbers.Substring(numbers.IndexOf(multipleCustomDelimitersId) + multipleCustomDelimitersId.Length, numbers.IndexOf(multipleCustomDelimitersSeperator) - (multipleCustomDelimitersSeperator.Length + 1));

                return delimiters.Split(new[] { multipleCustomDelimitersSplitter }, StringSplitOptions.RemoveEmptyEntries);
            }
            else if (numbers.StartsWith(customDelimiterId))
            {
                return new[] { numbers.Substring(numbers.IndexOf(customDelimiterId) + customDelimiterId.Length, numbers.IndexOf(newline) - (newline.Length + 1)) };
            }

            return new[] { ",", "\n" };
        }

        public string[] GetSortedDelimiters(string[] delimiters)
        {
            Array.Sort(delimiters, (x, y) => y.Length.CompareTo(x.Length));
            
            return delimiters;
        }
    }
}
